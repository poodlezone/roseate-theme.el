# roseate-theme.el
A somewhat pink theme for Emacs based on [the VSCode theme of the same name.](https://github.com/endorfina/roseate)
It's not *really* the same, but it's close enough to look pretty.
