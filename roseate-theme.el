;;; roseate-theme.el --- roseate
;;; Version: 1.0
;;; Commentary:
;;; A kinda pink theme for Emacs.
;;; Code:

(deftheme roseate "A kinda pink theme for Emacs.")
  (custom-theme-set-faces 'roseate
   '(default ((t (:foreground "#fff0f5" :background "#2c2833"))))
   '(cursor ((t (:background "#ffedf0"))))
   '(fringe ((t (:background "#2c2833"))))
   '(mode-line ((t (:foreground "#fde3e8" :background "#502b3c" ))))
   '(mode-line-emphasis ((t (:foreground "#ff5370" :background "#502b3c"))))
   '(mode-line-highlight ((t (:foreground "#ff5370" :background "#502b3c"))))
   '(mode-line-inactive((t (:foreground "#fde3e8" :background "#4f4355"))))
   '(doom-modeline-buffer-modified ((t (:foreground "#ff5370" :background "#502b3c"))))
   '(region ((t (:background "#372f3c"))))
   '(secondary-selection ((t (:background "#4f4355"))))
   '(font-lock-builtin-face ((t (:foreground "#ff5370"))))
   '(font-lock-comment-face ((t (:foreground "#547a70" :italic t))))
   '(font-lock-function-name-face ((t (:foreground "#7cc9b2"))))
   '(font-lock-keyword-face ((t (:foreground "#d779b6"))))
   '(font-lock-string-face ((t (:foreground "#c7b2a0"))))
   '(font-lock-type-face ((t (:foreground "#ce81d8"))))
   '(font-lock-constant-face ((t (:foreground "#ee3b9b"))))
   '(font-lock-variable-name-face ((t (:foreground "#eeffff"))))
   '(minibuffer-prompt ((t (:foreground "#fff0f5" :bold t))))
   '(font-lock-warning-face ((t (:foreground "#ff5370" :bold t))))
   '(line-number-current-line ((t (:foreground "#ff5370" :bold t))))
   )

;;;###autoload
(and load-file-name
    (boundp 'custom-theme-load-path)
    (add-to-list 'custom-theme-load-path
                 (file-name-as-directory
                  (file-name-directory load-file-name))))
;; Automatically add this theme to the load path

(provide-theme 'roseate)

;;; roseate-theme.el ends here
